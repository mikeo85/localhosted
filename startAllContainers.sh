#!/bin/bash
############################################################
#
#  START ALL CONTAINERS
#  ---------------------------------------------------------
#  Quickly start all containers in the repo
#
#        Written by: Mike Owens
#        Email:      mikeowens (at) fastmail (dot) com
#        Website:    https://michaelowens.me
#        GitLab:     https://gitlab.com/mikeo85
#        GitHub:     https://github.com/mikeo85
#        Twitter:    https://twitter.com/quietmike8192
#
#  Simple script to automatically start (or stop) all tools
#  in this repo. Just add the relative path of the Compose
#  file to the appropriate list below.
#
#  The script will start the reverse proxy service first,
#  then any other compose files in the order listed.
#
#  Vice versa for stopping the containers. Projects come
#  down first, then the reverse proxy.
#
#  Use the argument 'up' to start the containers.
#  Use the argument 'down' to stop the containers.
#
#  Examples:
#       $ ./startAllContainers.sh up
#       $ ./startAllContainers.sh down
#
############################################################

# //==== LIST PATHS TO DOCKER-COMPOSE FILES ====\\
# Paths are relative. Use double quotes for all paths.

# Path to the REVERSE PROXY compose file:
reverseProxy="./reverseProxy/docker-compose.yml"

# Paths to other compose files in repo (each on separate line):
othersList=(
    # "./reverseProxy/whoami-compose.yml"
    "./sui/docker-compose.yml"
    "./cyberchef/docker-compose.yml"
    "./corteza/docker-compose.yml"
    "./baserow/docker-compose.yml"
)
# \\===== END LIST OF DOCKER-COMPOSE FILES =====//

# //========== BEGIN INPUT VALIDATION ==========\\

## Input Validation Functions
## ----------------------------------------
exitHelp() { echo >&2 "$@";printf "%s" "$helpText";exit 1; }
countInputs() { [ "$#" -eq "$requiredNumOfInputs" ] || exitHelp "$requiredNumOfInputs argument(s) required, $# provided"; }
isNum() { echo "$1" | grep -E -q '^[0-9]+$' || exitHelp "Numeric argument required, \"$1\" provided"; }
inList() { local ckInput="$1"; shift; local arr=("$@"); printf '%s\n' "${arr[@]}" | grep -P -q "^$ckInput\$" || exitHelp "\"$ckInput\" is not a valid argument."; }

## Input Validation Process
## ----------------------------------------
## ++++ TO BE MODIFIED BY SCRIPT AUTHOR ++++

### Define the Help Text With Usage Instructions
helpText="Usage: $(basename "$0") starts and stops the docker containers for projects in this directory.
       $ $0 <up or down>
"
### Define Input Requirements
### ----------------------------------------
#### Indicate the required number of inputs
requiredNumOfInputs=1
##### List the valid inputs for each field.
##### Repeat for each field as needed. Remove if not required.
# validInputs1=( 1 2 )

### Validate The Inputs
### ----------------------------------------
countInputs "$@" # Were the expected number of inputs provided?
# inList "$1" "${validInputs1[@]}" # check input 1

# \\=========== END INPUT VALIDATION ===========//

# //============================================\\
# \\============================================//

echo "Running $(basename "$0")..."
# //============ BEGIN SCRIPT BODY =============\\
startDir="$PWD"
if [ "$1" == "up" ]; then
    echo "++++++++++ processing $reverseProxy"
    f=$(basename $reverseProxy)
    d=$(echo $reverseProxy | sed 's:/[-_a-zA-Z0-9]*.yml::')
    cd "$d"
    docker-compose -f "$f" up -d
    cd "$startDir"
    echo "++++++++++ completed $reverseProxy"
    for i in "${othersList[@]}"; do
        echo "++++++++++ processing $i"
        f=$(basename $i)
        d=$(echo $i | sed 's:/[-_a-zA-Z0-9]*.yml::')
        cd "$d"
        docker-compose -f "$f" up -d
        cd "$startDir"
        echo "++++++++++ completed $i"
    done
fi

if [ "$1" == "down" ]; then
    for i in "${othersList[@]}"; do
        echo "++++++++++ processing $i"
        f=$(basename $i)
        d=$(echo $i | sed 's:/[-_a-zA-Z0-9]*.yml::')
        cd "$d"
        docker-compose -f "$f" down
        cd "$startDir"
        echo "++++++++++ completed $i"
    done
    echo "++++++++++ processing $reverseProxy"
    f=$(basename $reverseProxy)
    d=$(echo $reverseProxy | sed 's:/[-_a-zA-Z0-9]*.yml::')
    cd "$d"
    docker-compose -f "$f" down
    cd "$startDir"
    echo "++++++++++ completed $reverseProxy"
fi
# \\============= END SCRIPT BODY ==============//
echo "$(basename "$0") complete."


# echo "Running "$(basename "$0")"..."
# # //----- Begin Script -----\\

# echo "########## Starting Traefik container and cybertools network ##########"
# docker-compose -f 01_traefik-compose.yml up -d
# echo "~~~~~~~~~~ Traefik container and cybertools network up ~~~~~~~~~~"

# for file in $(ls .)
# do
#     if [ "${file: -11}" == "compose.yml" ] && [ "$file" != "01_traefik-compose.yml" ] && [ "$file" != "02_template-compose.yml" ]; then
#         filename="$(basename $file '-compose.yml')"
#         echo "########## Starting $filename ##########"
#         docker-compose -f $(basename $file) up -d
#         echo "~~~~~~~~~~ $filename done ~~~~~~~~~~"
#     fi
# done

# # \\------ End Script ------//
# echo $(basename "$0")" complete."
